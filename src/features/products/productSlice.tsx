import { createSlice } from '@reduxjs/toolkit';
import intialStateProducts from '../../utils/initialStateProducts';

export const productSlice = createSlice({
  name: 'products',
  initialState: intialStateProducts,
  reducers: {
    addProduct: (state, action) => {
      state.push(action.payload);
    },
    deleteProduct: (state, action) => {
      const productFound = state.find(
        (product) => product.id === action.payload
      );

      if (productFound) {
        state.splice(state.indexOf(productFound), 1);
      }
    },
    editProduct: (state, action) => {
      const {
        id,
        SKU,
        name,
        shortDescription,
        description,
        imageUrl,
        category,
        price,
        mark,
        partNumber,
        family,
        engine,
        supplier,
        status,
      } = action.payload;
      const foundProduct = state.find((product) => product.id === id);

      if (foundProduct) {
        foundProduct.id = id;
        foundProduct.SKU = SKU;
        foundProduct.name = name;
        foundProduct.shortDescription = shortDescription;
        foundProduct.description = description;
        foundProduct.imageUrl = imageUrl;
        foundProduct.category = category;
        foundProduct.price = price;
        foundProduct.mark = mark;
        foundProduct.partNumber = partNumber;
        foundProduct.family = family;
        foundProduct.engine = engine;
        foundProduct.supplier = supplier;
        foundProduct.status = status;
      }
    },
  },
});

export const { addProduct, deleteProduct, editProduct } = productSlice.actions;
export default productSlice.reducer;
