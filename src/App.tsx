import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import ProductForm from './components/productForm';
import ProductInfo from './components/productInfo';
import ProductsList from './components/productsList';

function App() {
  return (
    <div className="gb-zinc-900 h-screen">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ProductsList />} />
          <Route path="/create" element={<ProductForm />} />
          <Route path="/edit-product/:id" element={<ProductForm />} />
          <Route path="/:id" element={<ProductInfo />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
