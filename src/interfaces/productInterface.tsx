export interface ProductsType {
  id: string;
  SKU: string;
  name: string;
  shortDescription: string;
  description: string;
  imageUrl: string;
  category: string;
  price: string;
  mark: string;
  partNumber: string;
  family: string;
  engine: string;
  supplier: string;
  status: string;
}
