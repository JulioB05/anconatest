import { combineReducers } from '@reduxjs/toolkit';
import { legacy_createStore as createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import productReducer from '../features/products/productSlice';

const persistConfiguration = {
  key: 'root',
  storage,
};

const reducer = combineReducers({
  products: productReducer,
});

const persistReducerApp = persistReducer(persistConfiguration, reducer);

export const store = createStore(persistReducerApp);

export const persistor = persistStore(store);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
