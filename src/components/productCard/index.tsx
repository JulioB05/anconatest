import { faPenToSquare, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { deleteProduct } from '../../features/products/productSlice';

interface ProductCardProps {
  name: string;
  imageUrl: string;
  shortDescription: string;
  SKU: string;
  id: string;
  price: string;
  mark: string;
  supplier: string;
}

const ProductCard = (props: ProductCardProps) => {
  const { imageUrl, name, shortDescription, SKU, id, price, mark, supplier } =
    props;

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleDelete = (id: string) => {
    dispatch(deleteProduct(id));
  };

  const handleEdit = (id: string) => {
    navigate(`/edit-product/${id}`);
  };

  const handleMoreInformation = (id: string) => {
    navigate(`/${id}`);
  };

  return (
    <div className="max-w-sm rounded overflow-hidden shadow-lg  hover:border-gray-100  hover:shadow-none cursor-pointer p-2">
      <div className="px-6 pt-4 pb-2 flex justify-end">
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
          SKU: {SKU}
        </span>
        <FontAwesomeIcon
          icon={faPenToSquare}
          className="px-2 w-6 h-6 cursor-pointer"
          style={{ color: '11426D' }}
          onClick={() => handleEdit(id)}
        />

        <FontAwesomeIcon
          icon={faTrash}
          className="px-2 w-6 h-6 cursor-pointer "
          style={{ color: 'F94949' }}
          onClick={() => handleDelete(id)}
        />
      </div>

      <div className="flex items-center justify-center">
        <img
          className="w-60 h-auto items-center justify-center"
          src={
            imageUrl
              ? imageUrl
              : 'https://t4.ftcdn.net/jpg/04/73/25/49/360_F_473254957_bxG9yf4ly7OBO5I0O5KABlN930GwaMQz.jpg'
          }
          alt=""
          onClick={() => handleMoreInformation(id)}
        />
      </div>
      <div className="px-6 py-4">
        <div className="font-bold text-xl mb-2">{name}</div>
        <p className="text-gray-700 text-base">{shortDescription}</p>
      </div>
      <div className="px-6 pt-4 pb-2">
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
          Price: {price}
        </span>
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
          Mark: {mark}
        </span>
        <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
          Supplier: {supplier}
        </span>
      </div>
    </div>
  );
};

export default ProductCard;
