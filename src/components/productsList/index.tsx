import { useSelector } from 'react-redux';
import { ProductsType } from '../../interfaces/productInterface';
import Header from '../header';
import ProductCard from '../productCard';

interface stateData {
  products: ProductsType[];
}

const ProductsList = () => {
  const state = useSelector((state) => state);
  const typedState = state as stateData;

  const { products } = typedState;

  return (
    <div className=" relative h-screen text-center md:text-left md:flex-row  mx-auto items-center justify-center md:justify-evenly">
      <Header />

      <h1 className="uppercase tracking-[20px] text-gray-900 text-2xl  text-center pt-40 pb-4">
        Product List
      </h1>

      <div>
        <div className="flex flex-wrap  container mx-auto  items-center justify-center">
          {products?.map((product, i) => {
            const {
              name,
              SKU,
              imageUrl,
              shortDescription,
              id,
              price,
              mark,
              supplier,
            } = product;
            return (
              <ProductCard
                key={i}
                name={name}
                imageUrl={imageUrl}
                shortDescription={shortDescription}
                SKU={SKU}
                id={id}
                price={price}
                mark={mark}
                supplier={supplier}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ProductsList;
