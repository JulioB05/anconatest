import { faPenToSquare } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ChangeEvent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { v4 as uuid } from 'uuid';
import { addProduct, editProduct } from '../../features/products/productSlice';
import { ProductsType } from '../../interfaces/productInterface';
import Header from '../header';

interface stateData {
  products: ProductsType[];
}

const ProductInfo = () => {
  const [product, setProduct] = useState({
    id: '',
    SKU: '',
    name: '',
    shortDescription: '',
    description: '',
    imageUrl: '',
    category: '',
    price: '',
    mark: '',
    partNumber: '',
    family: '',
    engine: '',
    supplier: '',
    status: '',
  } as ProductsType);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useParams();
  const state = useSelector((state) => state);
  const typedState = state as stateData;

  const { products } = typedState;

  useEffect(() => {
    if (params.id) {
      const data = products.find((product) => product.id === params.id);
      if (data) {
        setProduct(data);
      }
    }
  }, []);

  const handleChangeInputs = (e: ChangeEvent<HTMLInputElement>) => {
    setProduct({
      ...product,
      [e.target.name]: e.target.value,
    });

    if (e.target.name === 'imageUrl') {
      if (e.target.value === '') {
        setProduct({
          ...product,
          [e.target.name]:
            'https://t4.ftcdn.net/jpg/04/73/25/49/360_F_473254957_bxG9yf4ly7OBO5I0O5KABlN930GwaMQz.jpg',
        });
      } else {
        setProduct({
          ...product,
          [e.target.name]: e.target.value,
        });
      }
    } else {
      setProduct({
        ...product,
        [e.target.name]: e.target.value,
      });
    }
  };
  const handleChangeTextArea = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setProduct({
      ...product,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (params.id) {
      dispatch(editProduct(product));
    } else {
      dispatch(
        addProduct({
          ...product,
          id: uuid(),
        })
      );
    }
    navigate('/');
  };

  const handleEdit = (id: string) => {
    navigate(`/edit-product/${id}`);
  };
  return (
    <>
      <Header />

      <h1 className="uppercase tracking-[20px] text-gray-900 text-2xl  text-center pt-40 pb-5">
        {product.name}
        <FontAwesomeIcon
          icon={faPenToSquare}
          className="px-2 w-8 h-8 cursor-pointer"
          style={{ color: '001BFF' }}
          onClick={() => handleEdit(product?.id)}
        />
      </h1>

      <div className="flex flex-col relative  text-center md:text-left md:flex-row w-full px-10 mx-auto items-center justify-center md:justify-evenly">
        <div className="h-60 w-60">
          <img src={product.imageUrl} alt="" />
        </div>
        <form className="p-4" onSubmit={(e) => handleSubmit(e)}>
          <div className="grid gap-6 mb-6 md:grid-cols-5">
            <div>
              <label
                htmlFor="SKU"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                SKU
              </label>
              <input
                type="text"
                name="SKU"
                className="bg-gray-50 border border-gray-300 text-white text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="SKU-546"
                required
                onChange={(e) => handleChangeInputs(e)}
                value={product?.SKU}
              />
            </div>
            <div>
              <label
                htmlFor="name"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Name
              </label>
              <input
                type="text"
                name="name"
                className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Balero"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.name}
                required
              />
            </div>
            <div>
              <label
                htmlFor="category"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Category
              </label>
              <input
                type="text"
                name="category"
                className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Balero doble"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.category}
                required
              />
            </div>
            <div>
              <label
                htmlFor="mark"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Mark
              </label>
              <input
                type="text"
                name="mark"
                className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="KGV"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.mark}
                required
              />
            </div>
            <div>
              <label
                htmlFor="partNumber"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Number of Part
              </label>
              <input
                type="text"
                name="partNumber"
                className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="665465-asa-654"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.partNumber}
                required
              />
            </div>
            <div>
              <label
                htmlFor="Family"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Family
              </label>
              <input
                type="text"
                name="family"
                className="bg-gray-50 border border-gray-300 text-white text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Ford Fiesta 2012"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.family}
                required
              />
            </div>
            <div>
              <label
                htmlFor="engine"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Engine
              </label>
              <input
                type="text"
                name="engine"
                className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="1.6L"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.engine}
                required
              />
            </div>
            <div>
              <label
                htmlFor="price"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Price
              </label>
              <input
                type="text"
                name="price"
                className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="$599.99"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.price}
                required
              />
            </div>
            <div>
              <label
                htmlFor="supplier"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Supplier
              </label>
              <input
                type="text"
                name="supplier"
                className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="Ancona"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.supplier}
                required
              />
            </div>
            <div>
              <label
                htmlFor="status"
                className="block mb-2 text-sm font-medium text-gray-900"
              >
                Status
              </label>
              <input
                type="text"
                name="status"
                className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="5 pieces available"
                onChange={(e) => handleChangeInputs(e)}
                value={product?.status}
                required
              />
            </div>
          </div>

          <div className="mb-6">
            <label
              htmlFor="shortDescription"
              className="block mb-2 text-sm font-medium text-gray-900"
            >
              Short Description
            </label>
            <input
              type="text"
              name="shortDescription"
              className="bg-gray-50 border border-gray-300 text-white  text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Lorem Ipsum is simply dummy text"
              required
              onChange={(e) => handleChangeInputs(e)}
              value={product?.shortDescription}
            />
          </div>

          <div className="mb-6">
            <label
              htmlFor="description"
              className="block mb-2 text-sm font-medium text-gray-900"
            >
              Description
            </label>
            <textarea
              name="description"
              className="bg-gray-50 border border-gray-300 text-white text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:focus:ring-blue-500 dark:focus:border-blue-500 h-24"
              placeholder="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
              required
              onChange={(e) => handleChangeTextArea(e)}
              value={product?.description}
            />
          </div>
        </form>
      </div>
    </>
  );
};

export default ProductInfo;
