This is a test carried out for the vacancy as a Front end developer.

It consists of a display of products where you can perform the following actions:
• Add new products.
• View existing products.
• Modify existing products.
• Delete existing products.

The fields available for each article are the following:
1.SKU 
2. Name 
3. Short description 
4. Description 
5. Images 
6. Category 
7. Price 
8. Brand 
9. Part number 
10. Family
11.Engine 
12. Provider
13.Status

Vercel Implementation: https://anconatest.vercel.app/
